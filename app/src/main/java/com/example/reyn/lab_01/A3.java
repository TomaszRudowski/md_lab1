package com.example.reyn.lab_01;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class A3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a3);
    }

    /**
     * on click for button B3, formats result and closes activity
     * https://stackoverflow.com/questions/920306/sending-data-back-to-the-main-activity-in-android
     * @param view
     */
    public void b3onClick (View view) {
        Intent resultToA2 = new Intent();
        EditText textAtT4 = (EditText) findViewById(R.id.T4);
        String returnMessage = textAtT4.getText().toString();

        resultToA2.putExtra("returnValue", returnMessage);
        setResult(Activity.RESULT_OK, resultToA2);
        finish();
    }
}
