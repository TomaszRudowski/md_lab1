package com.example.reyn.lab_01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class A2 extends AppCompatActivity {

    static final int A2_TO_A3_REQUEST = 1;  // The request code

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a2);

    // Get the Intent that started this activity and extract the string  (Android tutorial)
        Intent intent = getIntent();
        String message = intent.getStringExtra(A1.EXTRA_MESSAGE);

    // Initialize T2 with name given in A1.T1
        TextView textView = findViewById(R.id.T2);
        if (message.isEmpty()) {
            textView.setText("Name not given");
        } else {
            textView.setText("Hello " + message);
        }
    }

    /**
     * on click action for button B2, sends request and starts activity A3
     * @param view
     */
    public void b2onClick (View view) {
        Intent intent = new Intent(this, A3.class);
        startActivityForResult(intent,A2_TO_A3_REQUEST);
    }

    /**
     * Action to results received from A3, based on Android tutorial
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == A2_TO_A3_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                // The user clicked on button B3.
                // The Intent's data Uri identifies what was in T4 editText field.
                String returnedValue = data.getStringExtra("returnValue");
                // Do something with the returned value
                TextView textView = findViewById(R.id.T3);
                if (returnedValue.isEmpty()) {
                    textView.setText("Empty message from A3");
                } else {
                    textView.setText("From A3: " + returnedValue);
                }
            } else {
                TextView textView = findViewById(R.id.T3);
                textView.setText("Activity A3 canceled");
            }
        }
    }
}
