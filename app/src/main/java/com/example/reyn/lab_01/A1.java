package com.example.reyn.lab_01;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class A1 extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public static final String EXTRA_MESSAGE = "com.example.reyn.lab_01.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a1);

        Spinner spinner = (Spinner) findViewById(R.id.L1);
    // Create an ArrayAdapter using the string array and a default spinner layout (Android tutorial)
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.planets_array, android.R.layout.simple_spinner_item);
    // Specify the layout to use when the list of choices appears (Android tutorial)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    // Apply the adapter to the spinner (Android tutorial)
        spinner.setAdapter(adapter);

        // adds selected item listener (Android tutorial)
        spinner.setOnItemSelectedListener(this);

        int preferences = readIntPreferences("planet");
        if (preferences > -1) {
            spinner.setSelection(preferences);      // default value -1 if preferences not found
        }

    }

    /**
     * on click action for button B1
     * based on Android tutorial
     * @param view
     */
    public void b1onClick (View view) {
        Intent intent = new Intent(this, A2.class);
        EditText textAtT1 = (EditText) findViewById(R.id.T1);
        String message = textAtT1.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    /**
     * needed for "implements AdapterView.OnItemSelectedListener"
     * @param mySpinner
     * @param view
     * @param position
     * @param l
     */
    @Override
    public void onItemSelected(AdapterView<?> mySpinner, View view, int position, long l) {
        //String chosenPlanet = mySpinner.getItemAtPosition(position).toString();
        storePreferances("planet",position);
    }

    /**
     * not used, needed for "implements AdapterView.OnItemSelectedListener"
     * from Android tutorial
     * @param adapterView
     */
    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    /**
     * version to store string (from imt3673 wiki)
     * @param key
     * @param value
     */
    private void storePreferances (String key, String value) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * version to store int
     * @param key
     * @param value
     */
    private void storePreferances (String key, int value) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    /**
     * read preferences as String (from imt3673 wiki)
     * @param key
     * @return
     */
    private String readStringPreferences(String key) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final String value = prefs.getString(key, "");  // default value "" if preferences not found
        return value;
    }

    /**
     * read preferences as Int
     * @param key
     * @return
     */
    private int readIntPreferences(String key) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final int pos = prefs.getInt(key, -1);  // default value -1 if preferences not found
        return pos;
    }
}
